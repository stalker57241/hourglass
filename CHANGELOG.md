# 0.8.0 (2020-11-08)

- Fix sorting order in the Versions tab
- Fix sorting in the version edit dropdown
- Other minor fixes
